from Scrappy import overlookArticle as scan
from bs4 import BeautifulSoup
import os
import errno
import requests
import datetime

def start():
    
    print('STARTTIME:')
    print(datetime.datetime.now())
    print(' ')
    print('===================INITIALIZED===================')

    topics = dict()
    topics["Schweiz"] = ["https://www.20min.ch/schweiz/", "https://www.20min.ch/schweiz/zuerich", "https://www.20min.ch/schweiz/basel", "https://www.20min.ch/schweiz/bern", "https://www.20min.ch/schweiz/zentralschweiz/", "https://www.20min.ch/schweiz/ostschweiz/", "https://www.20min.ch/schweiz/energychallenge/"]
    topics["Ausland"] = ["https://www.20min.ch/ausland/"]
    topics["Wirtschaft"] = ["https://www.20min.ch/finance/"]
    topics["Sport"] = ["https://www.20min.ch/sport/"]
    topics["People"] = ["https://www.20min.ch/people/", "https://www.20min.ch/people/schweiz/", "https://www.20min.ch/people/international/"]
    topics["Entertainment"] = ["https://www.20min.ch/entertainment/", "https://www.20min.ch/entertainment/musik/", "https://www.20min.ch/entertainment/tv/" ,"https://www.20min.ch/entertainment/kino/"]
    topics["Digital"] = ["https://www.20min.ch/digital/", "https://www.20min.ch/digital/news/", "https://www.20min.ch/digital/e-sport/", "https://www.20min.ch/digital/games/"]
    topics["Wissen"] = ["https://www.20min.ch/wissen/", "https://www.20min.ch/wissen/news/", "https://www.20min.ch/wissen/history/", "https://www.20min.ch/wissen/gesundheit/", "https://www.20min.ch/wissen/karriere/"]
    topics["Lifestyle"] = ["https://www.20min.ch/leben/", "https://www.20min.ch/kochenmitfooby/", "https://www.20min.ch/leben/reisen/", "https://www.20min.ch/bodyandsoul/", "https://www.20min.ch/homes/", "https://www.20min.ch/motor/", "https://www.20min.ch/wohnen/"]
    topics["Viral"] = ["https://www.20min.ch/community/viral/"]
    

    for topic, links in topics.items():
        # Ordner erstellen
        try:
            os.makedirs("Artikel/" + topic)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
            else:
                pass
        print('==============BEGINNING OF ' + topic +  '===============')
        for link in links:
            response = requests.get(link, timeout=100)
            content = BeautifulSoup(response.content, "html.parser")
            LinkList =[]

            ArticleProCat = content.select("div.teaser_title.front > h2 > a")
            for links in ArticleProCat:
                FinalLinks = links.get('href')
                LinkList.extend(["https://www.20min.ch"+FinalLinks])
        

            websites = LinkList
            for website in websites:
                scan(website, topic)

        if topic == "Viral":
            print('END OF ' + topic)
            print('CYCLE COMPLETE')
            print(datetime.datetime.now())
        else:
            print('====================END OF ' + topic + '===============')
            print(' ')


        