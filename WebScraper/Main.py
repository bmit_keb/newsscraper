import requests
import Scanner
import schedule
import time
from bs4 import BeautifulSoup 

Scanner.start()

schedule.every(60).seconds.do(Scanner.start)

while True:
    schedule.run_pending()
    time.sleep(1)
 