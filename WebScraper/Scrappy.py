import requests
from bs4 import BeautifulSoup 
import Save

def uniquify(seq, idfun=None): 
     # order preserving
    if idfun is None:
        def idfun(x): return x
    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        # in old Python versions:
        # if seen.has_key(marker)
        # but in new ones:
        if marker in seen: continue
        seen[marker] = 1
        result.append(item)
    return result



def overlookArticle(url, category):
    try:
        response = requests.get(url, timeout=100)
    except:
        return
    content = BeautifulSoup(response.content, "html.parser")

    artikeltext = ""
    #inhalt zeilenweise auslesen
    for inhalt in content.findAll('p'):
        artikeltext = artikeltext+" "+(inhalt.text)

    #prüfen ob auf dem Artikel Kommentäre freigeschalten sind
    if (content.find('div', attrs={"id": "talkback"}) is not None):
        hasComments = True
        try:
            bestComment = content.find('ul', attrs={"class":"mostLikedComments"}).find('div', attrs={"class":"entry"}).find('p').text
        except:
            bestComment = "error"
    else:
        hasComments = False
        bestComment = "No Comment"


    wordlist = artikeltext.split()
    bigWordList = [x for x in wordlist if x.istitle() or x.isupper()]
    wordlist = uniquify(bigWordList, lambda word: word.lower())

    occurences = dict()

    for word in wordlist:
        amount = artikeltext.count(word)
        occurences[word] = amount

    sortedDict = sorted(occurences.items(), reverse=True, key=lambda x: x[1])


    subcategory = "Unbekannt"

    subCategoryElements = content.select("[id=navi_bar] > [id=subnavigation] > [class=active]")
    if len(subCategoryElements) is not 0:
        subcategory = subCategoryElements[0]

    ArticleDate = "Unknown"
    ArticleF5 = "Unknown"
    try:
        ArticleDate = content.find("div", "published","clearfix").find("p").text
        dateIndex = ArticleDate.find(";")
        ArticleDate = ArticleDate[0:dateIndex]

        ArticleF5 = content.find("div", "published","clearfix").find("p").find("span").text
    except:
        pass

    
    author = "Unknown"
    try:
        author = content.find('p', attrs={"class": "autor"}).text
    except:
        pass

#als array
    articleObjekt ={
        "Author": author,
        "Erstellungs_Datum": ArticleDate,
        "Aktualisierungs_Datum": ArticleF5,
        "Title": content.find('h1').text,
        "Tagline": content.find('h4').text,
        "Category": category,
        "Subcategory": subcategory,
        "Words": sortedDict,
         #"Text": artikeltext,
        "Comments": hasComments,
        "Best_Comment": bestComment
    }

    # Artikel in JSON Format auf die Festplatte speichern    

    Save.toJsonFile(articleObjekt)