import json
import sys
import os
import unicodedata
from django.utils.text import slugify

def newFile(name, iterator):
    if (iterator == 1):
        return name + ".json"
    else:
        return name + "(" + str(iterator) + ")" + ".json"


def toJsonFile(article):
    new = False
    headline = article.get("Title")
    category = article.get("Category")
    #try:
    headline = slugify(headline)
    fNameNoExt = 'Artikel/' + category + "/" + headline
    filename = fNameNoExt + '.json'
    # letztes file suche
    if os.path.isfile(filename):
            iterator = 2
            newFileName = newFile(fNameNoExt, iterator)
            
            while os.path.isfile(newFileName):
                new = True
                filename = newFileName
                iterator += 1
                newFileName = newFile(fNameNoExt, iterator)
            iterator = iterator - 1
            filename = newFile(fNameNoExt, iterator)
            
        #überprüfung ob aktualisierungsdatum gleich
    #try:
            with open(filename, encoding="utf8") as toCheck:
                articleObject = json.load(toCheck)
                lastChange = articleObject.get("Aktualisierungs_Datum")
                currentChange = article.get("Aktualisierungs_Datum")

                if (lastChange == currentChange):
                    #print('Article skipped: ', headline)
                    return
            filename = newFileName
    #except:
        #return       
    with open(filename, "w", encoding='utf-8') as file:
        json.dump(article, default=lambda o: o.__o__.dict__, sort_keys=True, indent=4, ensure_ascii=False, fp=file)
    if new:
        print("Article updated: ", headline)
        return
    print("Article added: ", headline)
   # except:
    #    print("Could not read: " + headline)
